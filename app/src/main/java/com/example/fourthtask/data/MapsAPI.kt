package com.example.fourthtask.data

import retrofit2.http.GET
import retrofit2.Call

interface MapsAPI {
    @GET("./api/atm?city=Гомель ")
    fun getATMList(): Call<List<ATMItems>>
}