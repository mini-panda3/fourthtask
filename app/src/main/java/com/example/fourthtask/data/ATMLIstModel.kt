package com.example.fourthtask.data

import com.google.gson.annotations.SerializedName

data class ATMItems(
    @SerializedName("address_type")
    val addres_type: String,

    @SerializedName("address")
    val addres: String,

    @SerializedName("house")
    val house: String,

    @SerializedName("gps_x")
    val gps_x: String,

    @SerializedName("gps_y")
    val gps_y: String,

    @SerializedName("ATM_type")
    val ATMType: String

)