package com.example.fourthtask

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.example.fourthtask.data.ATMItems
import com.example.fourthtask.data.MapsAPI
import com.example.fourthtask.data.MyClass
import com.example.fourthtask.data.networkConnection

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.example.fourthtask.databinding.ActivityMapsBinding
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    lateinit var clusterManager: ClusterManager<MyClass>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val gomel = LatLng(52.4345 , 30.9754)
        clusterManager = ClusterManager(this, mMap)

        val alertBuilder = AlertDialog.Builder(this)

        val networkConnection = networkConnection(applicationContext)
        networkConnection.observe(this, Observer {isConnected ->

            if (isConnected) {
                retrofitBuilder()
            } else {
                alertBuilder.setTitle("Map application")
                alertBuilder.setMessage("No internet connection")
                alertBuilder.setNeutralButton("OK"){dialog, i ->
                    dialog.cancel()
                }
                alertBuilder.show()
            }

        })

        mMap.setOnCameraIdleListener(clusterManager)
        mMap.setOnMarkerClickListener(clusterManager)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, 12.5f))
    }


    private fun retrofitBuilder(){

        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://belarusbank.by")
            .build()
            .create(MapsAPI::class.java)

        val retrofitData = retrofit.getATMList()
        retrofitData.enqueue(object : Callback<List<ATMItems>?>{
            override fun onResponse( call: Call<List<ATMItems>?>, response: Response<List<ATMItems>?>) {
                val responseBody = response.body()!!
                for (ATMListModel in responseBody){
                    val Item = MyClass(ATMListModel.gps_x.toDouble(), ATMListModel.gps_y.toDouble(), ATMListModel.addres_type + ATMListModel.addres + " " + ATMListModel.house, "Тип: "+ATMListModel.ATMType)
                    clusterManager.addItem(Item)
                }
            }

            override fun onFailure(call: Call<List<ATMItems>?>, t: Throwable) {
                Log.d("MainActivity", "onFailure" + t.message)
            }
        }

        )
    }

}